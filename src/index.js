// Example with how to export your application
// const fastify = require('fastify')({ logger: false });
// fastify.get('/', async (request, reply) => {
//   return { hello: 'world' }
// });
//
// module.exports = async () => {
//   try {
//     await fastify.ready();
//   } catch (err) {
//     process.exit(1)
//   }
//   return {
//     server: fastify.server,
//     closeServer: async () => new Promise((resolve) => fastify.server.close(resolve)),
//   };
// };

module.exports = async () => {
  // TODO replace with real application instance
  return {
    server: null,
    closeServer: async () => {},
  };
};
